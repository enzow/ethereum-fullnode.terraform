variable "key_name" {
  description = "Name of the SSH keypair to use in AWS."
}

variable "chain_net" {
  description = "Blockchain to sync from"
  default     = "mainnet"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-southeast-1"
}

# ubuntu 16.04 LTS
variable "aws_amis" {
  default = {
    "ap-southeast-2" = "ami-33ab5251"
    "ap-southeast-1" = "ami-b7f388cb"
  }
}
