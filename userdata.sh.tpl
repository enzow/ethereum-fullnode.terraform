#!/bin/bash -v
apt-get update -y
apt-get install -y chrony sysstat
sudo systemctl enable chrony
sudo systemctl start chrony
mkfs.ext4 /dev/xvdh
sudo mkdir /data
echo '/dev/xvdh /data ext4 defaults,errors=remount-ro,noatime 0 2' >> /etc/fstab
sudo mount -a
chown ubuntu:ubuntu /data
bash <(curl https://get.parity.io -kL)
cat >>  /lib/systemd/system/parity.service <<EOF
[Unit]
Description=Parity Daemon
After=network.target

[Service]
# run as root, set base_path in config.toml
ExecStart=/usr/bin/parity --jsonrpc-apis all --chain=${chain_net} --jsonrpc-hosts all --jsonrpc-cors all --unsafe-expose --log-file /home/ubuntu/parity.log --db-path=/data/ethereum
# To run as user, comment out above and uncomment below, fill in user and group
# picks up users default config.toml in /root/.local/share/io.parity.ethereum/
User=ubuntu
Group=ubuntu
Type=simple
# ExecStart=/usr/bin/parity
Restart=on-failure

# Specifies which signal to use when killing a service. Defaults to SIGTERM.
# SIGHUP gives parity time to exit cleanly before SIGKILL (default 90s)
KillSignal=SIGHUP

[Install]
WantedBy=default.target
EOF

systemctl daemon-reload
systemctl enable parity
systemctl start parity
