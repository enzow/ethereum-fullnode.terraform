output "address" {
  value = "${aws_elb.parity-elb.dns_name}"
}
