# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
  profile = "cen_dev"
}

resource "aws_vpc" "default" {
  cidr_block  = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags {
    Name = "parity_dev"
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_subnet" "parity_dev_subnet" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true

  tags {
    Name = "parity_dev_subnet"
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Name = "parity_dev_ig"
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_route_table" "r" {
  vpc_id = "${aws_vpc.default.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "aws_route_table"
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = "${aws_subnet.parity_dev_subnet.id}"
  route_table_id = "${aws_route_table.r.id}"
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "parity_sg"
  description = "managed in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 30303
    to_port     = 30303
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 30303
    to_port     = 30303
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8545
    to_port     = 8546
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8545
    to_port     = 8546
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

# Our elb security group to access
# the ELB over HTTP
resource "aws_security_group" "elb" {
  name        = "elb_sg"
  description = "Used in the terraform"

  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port   = 8545
    to_port     = 8546
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8545
    to_port     = 8546
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # ensure the VPC has an Internet gateway or this step will fail
  depends_on = ["aws_internet_gateway.gw"]

  tags {
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_elb" "parity-elb" {
  name = "singularx-parity-elb"
  depends_on = ["aws_instance.paritynodes"]

  # The same availability zone as our instance
  subnets = ["${aws_subnet.parity_dev_subnet.id}"]

  security_groups = ["${aws_security_group.elb.id}"]

  listener = {
    instance_port     = 8545
    instance_protocol = "http"
    lb_port           = 8545
    lb_protocol       = "http"
  }

  listener = {
    instance_port     = 8546
    instance_protocol = "http"
    lb_port           = 8546
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:8545"
    interval            = 15
  }

  # The instance is registered automatically

  instances                   = ["${aws_instance.paritynodes.*.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 60
  connection_draining         = true
  connection_draining_timeout = 60

  tags {
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

resource "aws_instance" "paritynodes" {
  instance_type = "m4.xlarge"
  count = 1

  # Lookup the correct AMI based on the region
  # we specified
  ami = "${lookup(var.aws_amis, var.aws_region)}"

  # The name of our SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:
  #
  key_name = "${var.key_name}"

  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.default.id}"]
  subnet_id              = "${aws_subnet.parity_dev_subnet.id}"
  user_data              = "${data.template_file.parity_installation.rendered}"

  root_block_device {
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    device_name = "/dev/sdh"
    volume_size = 150
    volume_type = "gp2"
    delete_on_termination = false
  }

  #Instance tags

  tags {
    Name = "parity-node-dev"
    Owner = "singularX"
    Project = "fullnode"
    ENV = "dev"
  }
}

data "template_file" "parity_installation" {
  template = "${file("${path.module}/userdata.sh.tpl")}"

  vars {
    chain_net = "${var.chain_net}"
  }
}